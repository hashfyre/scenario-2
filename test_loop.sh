#!/bin/sh

# Oh! This might just be the infamous infinite loop!

a=4
while [ "$a" -lt 10 ]
do
   echo `expr $a + 1`
   a=`expr $a + 1` #- you should try removing the comment here to make the loop end
done

echo 'Done looping!'
